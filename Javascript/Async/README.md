[Promise](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Using_promises)    
[Asynchronous](https://www.w3schools.com/js/js_callback.asp)  
Promise nesting is a control structure to limit the scope of catch statements. Specifically, a nested catch only catches failures in its scope and below, not errors higher up in the chain outside the nested scope. When used correctly, this gives greater precision in error recovery.
