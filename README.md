# Samin's Reading Room  


### Data structure -
    Linked List
    Binary Heap
    Priority queue
    Tree
    Disjoint set
    Graph
    Hashing
    
    
### Algorithm -
    Sorting
    Pattern Matching
    Bisection
    Bit manipulation 
    Dynamic Programming
    miscellaneous
    Floyed cycle detection

### C/C++ -
    pointer
    C library function implementation
    
    
### Dart -
    Constructor injection
    Method injection
    
### Javascript -  

### Implementation
    Execution time in C++
    Pause
    
### uVa problem solution 


good website->
https://www.youtube.com/channel/UCSpQqfgiGQtcxR_8Ihy7oXg - Tanvir Sajor
Safayet blog
https://www.youtube.com/watch?v=OpebHLAf99Y&list=PL2_aWCzGMAwI9HK8YPVBjElbLbI3ufctn&index=3 ->mycodeschool  
Compiler explorer -> https://godbolt.org/  
